#! /bin/bash

archi=`lscpu | grep Architecture | awk '{ print $2 }'`

if [ $archi == "x86_64" ]; then
    echo 'Installation X86, npm install'
    cd ../node/
    npm install
else
    echo 'Installation ARM, copie du module'
    cp -rf node_modules ../node/
fi
