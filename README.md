jeedom_mysensors
================

=== Changelog ===

See : https://github.com/lunarok/jeedom_mysensors/blob/master/doc/fr_FR/changelog.asciidoc

=== Todo in plugin ===

Load sketch with avrdude / or integrate OTA -> this is frozen in the plugin, hard to define for everyone, look at MYSController for a good companion in this part

=== Ideas for nodes ===

Send libversion at presentation including for gateway

Data type is not needed and sensor types must be more globals (ie. IR_SEND must become REMOTE, no type, no reference to a technology)

Send sensor name in presentation payload

Send the power source by node, maybe in battery
